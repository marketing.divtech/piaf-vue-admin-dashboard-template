import firebase from "firebase/compat/app";
import "firebase/compat/auth";
import { currentUser, isAuthActive } from "../../constants/config";
import router from "../../router";
import Vue from "vue";

export default {
  state: {
    currentUser: isAuthActive
      ? currentUser
      : localStorage.getItem("user") != null
      ? JSON.parse(localStorage.getItem("user"))
      : null,
    // currentUser: null,
    loginError: null,
    processing: false,
    forgotMailSuccess: null,
    resetPasswordSuccess: null,
  },
  getters: {
    currentUser: (state) => state.currentUser,
    processing: (state) => state.processing,
    loginError: (state) => state.loginError,
    forgotMailSuccess: (state) => state.forgotMailSuccess,
    resetPasswordSuccess: (state) => state.resetPasswordSuccess,
  },
  mutations: {
    setUser(state, payload) {
      state.currentUser = payload;
      state.processing = false;
      state.loginError = null;
    },
    setLogout(state) {
      state.currentUser = null;
      state.processing = false;
      state.loginError = null;
    },
    setProcessing(state, payload) {
      state.processing = payload;
      state.loginError = null;
    },
    setError(state, payload) {
      state.loginError = payload;
      state.currentUser = null;
      state.processing = false;
    },
    setForgotMailSuccess(state) {
      state.loginError = null;
      state.currentUser = null;
      state.processing = false;
      state.forgotMailSuccess = true;
    },
    setResetPasswordSuccess(state) {
      state.loginError = null;
      state.currentUser = null;
      state.processing = false;
      state.resetPasswordSuccess = true;
    },
    clearError(state) {
      state.loginError = null;
    },
  },
  actions: {
    login({ commit }, payload) {
      commit("clearError");
      commit("setProcessing", true);
      firebase
        .auth()
        .signInWithEmailAndPassword(payload.email, payload.password)
        .then(
          (user) => {
            const item = { uid: user.user.uid, ...currentUser };
            localStorage.setItem("user", JSON.stringify(item));
            commit("setUser", { uid: user.user.uid, ...currentUser });
          },
          (err) => {
            localStorage.removeItem("user");
            commit("setError", err.message);
            setTimeout(() => {
              commit("clearError");
            }, 3000);
          }
        );
    },
    // JWT login
    // async login({ commit }, payload) {
    //   commit("clearError");
    //   commit("setProcessing", true);
    //   try {
    //     const response = await axios.post(
    //       `${process.env.VUE_APP_API_ENDPOINT}/login`,
    //       {
    //         user_name: payload.username,
    //         user_password: payload.password,
    //       }
    //     );
    //     const token = response.data.data;
    //     $cookies.set("token", token);
    //     router.push("/");
    //   } catch (error) {
    //     commit("setError", error.response.data.message);
    //   }
    // },
    forgotPassword({ commit }, payload) {
      commit("clearError");
      commit("setProcessing", true);
      firebase
        .auth()
        .sendPasswordResetEmail(payload.email)
        .then(
          (user) => {
            commit("clearError");
            commit("setForgotMailSuccess");
          },
          (err) => {
            commit("setError", err.message);
            setTimeout(() => {
              commit("clearError");
            }, 3000);
          }
        );
    },
    resetPassword({ commit }, payload) {
      commit("clearError");
      commit("setProcessing", true);
      firebase
        .auth()
        .confirmPasswordReset(payload.resetPasswordCode, payload.newPassword)
        .then(
          (user) => {
            commit("clearError");
            commit("setResetPasswordSuccess");
          },
          (err) => {
            commit("setError", err.message);
            setTimeout(() => {
              commit("clearError");
            }, 3000);
          }
        );
    },

    /*
       return await auth.(resetPasswordCode, newPassword)
        .then(user => user)
        .catch(error => error);
    */
    signOut({ commit }, reason) {
      if (reason === "timeout") {
        Vue.prototype.$notify(
          "error filled",
          "Session Timeout",
          "Please login again.",
          {
            duration: 3000,
            permanent: false,
          }
        );
      }
      commit("setLogout");
    },
  },
};
