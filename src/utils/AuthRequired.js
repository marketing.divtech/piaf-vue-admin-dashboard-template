import { isAuthActive } from '../constants/config'
export default (to, from, next) => {
  if (isAuthActive){
    next()
  }else{
    if (localStorage.getItem('user') != null && localStorage.getItem('user').length > 0) {
      // verify with firebase or jwt
      next()
    } else {
      localStorage.removeItem('user')
      next('/user/login')
    }
  }
}

// Using JWT
// import * as jwt from 'jsonwebtoken'
// import store from "../store";

// export default async (to, from, next) => {
//   const token = $cookies.get('token');
//   if (token) {
//     const decodedToken = jwt.decode(token);

//     if (decodedToken && decodedToken.exp * 1000 < Date.now()) {
//       // Token has expired
//       store.dispatch('signOut', 'timeout');
//     } else {
//       // Token is still valid
//       store.commit('setUser', decodedToken);
//       next();
//     }
//   } else {
//     // No token found
//     store.dispatch('signOut', 'timeout');
//   }
// }
